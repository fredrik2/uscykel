Följande Durpal 6 modul bygdes sommaren 2012 för att fungera som landningssida och för att automatisera närvaron på sociala medier under kampanjen US V'st - Göteborg - Uppsala på Cykel.

Kampanjen har sin bakgrund i att Riksorganisationen Unga med Synnedsättnngar (då Unga Synskadade) beslutat att alla distrikt skulle försöka ta sig till kongressen i Uppsala på ett uppseendeväckande vis som anknöt till kollektivtrafik. Då ledsagningen inte alltid fungerar för personer med synnedsättning spånade US Väst (Unga med Synnedsättningar i Västra Götalands län och Region Halland) fram en idé om att cykla hela sträckan, då detta är det kvaravarande miljövänliga sättet att resa. Till detta byggdes denna modul som utökade organisationens befintliga webbplats.

Modulen består av en nod samt block för meny, instagram-bilder och följ-knappar. Det finns också en task som körs tillsamans med Drupals övriga Cron-jobb som sköter uppdateringen av tweets och väderdata.

Noden visar en satelitbild från Google som zoomar in på var cykelkaravanen senast befann sig. Så fort positionen uppdateras från någon av deltagaranas smarta telefoner så flyttas bilden. Detta görs mned hjälp av tjänsten GeoLoqi. Adress hämtas från Bing då dessa hade förmånliga vilkor för ideélla organisationer.
Tasken kollar om positionen ändrats sedan senast och om det är dags att skicka ut en tweet eller uppdatera vädret. 