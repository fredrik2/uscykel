<?php

// Blocks for US Cykel

function uscykel_block($op='list', $delta=0) {

  if ($op == "list") {
    // Generate listing of blocks from this module, for the admin/block page
    $block = array();
    $block[0]["info"] = t('US Cykel Follow');
    $block[1]["info"] = t('US Cykel Instagram');
    return $block;
  }
  else if ($op == 'view') {
    // Subject but no content. Get content from file.
    switch($delta) {
      case 0:
        // Get the vucabulary list
        require_once("uscykel_block_follow.php");
        
        // Show the block
        $block = array('subject' => 'Följ Oss på resan!',
          'content' => uscykel_block_follow() );
      break;
      
      // Block for displaying the instagram feed.
      case 1:
        // Get the vucabulary list
        require_once("uscykel_block_instagram.php");
        
        // Show the block
        $block = array('subject' => 'Senaste bilderna',
          'content' => uscykel_block_instagram() );
      break;
    }

    return $block;
    
  }
}