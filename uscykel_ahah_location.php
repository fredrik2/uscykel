<?php

// This file will load location data from bing.

function uscykel_ahah_location() {
  // Load the adress info.
  $adress = uscykel_loadAdress(arg(3));
  
  // Load the current weather data from DB.
  $db_query = "SELECT max(id) as id FROM {uscykel_weather}";
  $db_result = db_query($db_query);
  $max_id = db_fetch_object($db_result);
  
  // Get the wheater for max id
  $db_query = "SELECT temperature, wind, rain, symbol FROM {uscykel_weather} WHERE id=".$max_id->id;
  $db_result = db_query($db_query);
  $wheater_data = db_fetch_object($db_result);
  
  // Create an array to return
  $return = array("place" => $adress["street_name"]."<br />".$adress["town"],
                  "w_symbol" => $wheater_data->symbol,
                  "w_temperature" => $wheater_data->temperature,
                  "w_wind" => $wheater_data->wind,
                  "w_rain" => $wheater_data->rain,                  
                  );
  
  
  // Return JSON
  drupal_json($return);
}