<?php 

// This is the block for the follow us buttons.
function uscykel_block_follow() {
  
  // Insert the facebook link.
  $output = "<a href=\"http://www.facebook.com/uscykel\" target=\"_BLANK\"><img src=\"".drupal_get_path('module', 'uscykel')."/img/facebook.png"."\" alt=\"Gilla på Facebook\"></a>";
  // Insert the Twitter link.
  $output .= "<a href=\"http://www.twitter.com/uscykel\" target=\"_BLANK\"><img src=\"".drupal_get_path('module', 'uscykel')."/img/twitter.png"."\" alt=\"Följ @USCykel på Twitter\"></a>";
  
  // Return the block
  return $output;
}