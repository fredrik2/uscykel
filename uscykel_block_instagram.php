<?php

// This file contains the block for displaying images from Instagram.
function uscykel_block_instagram(){
  // Get images from instagram
  $result = uscykel_getInstagram();
  $result = json_decode($result);
  
  // Run through the result four times
  for ($i=0; $i<3; $i++){
    // Show a thumbnail photo and link to instagram
    $output .= "<a href=\"".$result->data[$i]->link."\" target=\"_BLANK\">
                <img src=\"".$result->data[$i]->images->thumbnail->url."\" width=\"150\" height=\"150\" alt=\"Foto från Instagr.am\" style=\"margin-right: 5px;\"></a>";
  }
  
  // Return the output
  return $output;
}


// Cur function that gets data from instagram
function uscykel_getInstagram(){
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, "https://api.instagram.com/v1/users/187838410/media/recent/?access_token=187838410.4316961.30b5caf1e0c446658e57efec56897462");
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_TIMEOUT, 20);
  $result = curl_exec($ch);
  curl_close($ch); 
  return $result;
}