<?php

// Global functions for the US Cykel module.


// Function for getting the last position.
function uscykel_lastposition() {
  // Include the Geoloqi sdk
  require_once("geoloqi-php/Geoloqi.php");
  
  // Geoloqi info
  $app_id = "";
  $app_secret = "";
  
  // Do the oAuth to Geoloqi
  $geoloqi = new Geoloqi($app_id,
                         $app_secret,
                         '');
  $code = "";
  
  // Use the access token to login.
  $geoloqi = Geoloqi::createWithAccessToken($code);
  
  // Get latest location as result
  $response = $geoloqi->get('location/last');
  
  // Sava some data into an array
  $location_data["lat"] = $response->location->position->latitude;
  $location_data["long"] = $response->location->position->longitude;
  $location_data["speed"] = $response->location->position->speed;
  $location_data["altitude"] = $response->location->position->altitude;
  
  // Return $location_data
  return $location_data;
}


// Function for loading the adress.
function uscykel_loadAdress($LatLong){
  // Connect to bing to get the info.
    // Bing Key
  $bingKey = "";
  
  // create curl resource
  $ch = curl_init();
  
  // Create the URL
  $url = "http://dev.virtualearth.net/REST/v1/Locations/$LatLong?o=json&key=$bingKey";
  
  // set url
  curl_setopt($ch, CURLOPT_URL, $url);
  
  //return the transfer as a string
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  
  // $output contains the output string
  $data = curl_exec($ch);
  
  // close curl resource to free up system resources
  curl_close($ch); 
  
  // Make an array of JSON
  $location_info = json_decode($data, true);
  
  // Create an Array with the Street and Town name.
  // Get Street Neme.
  $street_name = $location_info["resourceSets"][0]["resources"][0]["address"]["addressLine"];
  
  // Check if the street_name is a numeric value.
  if (is_numeric($street_name)){
     // Add the word "väg" to $street_name
     $street = $street_name;
     $street_name = "Väg ".$street;
  }
  
  // Get the town (mucipality)
  $town = $location_info["resourceSets"][0]["resources"][0]["address"]["locality"];

  // Create the array and return it
  $location["street_name"] = $street_name;
  $location["town"] = $town;
  return $location;

}