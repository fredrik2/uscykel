<?php

// This is the main node showing the map and all its data.

function uscykel_mainnode(){
   // Get the current time as timestamp and check if we shall display the 
   // Real site or the count down.
   if (time() > strtotime("2012:07:20 09:59:45")){
     // Show The Real!
    
     // Load the latest known position so that it can be shown to the user when 
     // the map is loaded.
     $last_position = uscykel_lastposition();
     
     // Add external JavaScript to <head>-tag
     $html_head_data = "
        <!-- Include Map Data -->
        <script type=\"text/javascript\" src=\"http://maps.google.com/maps/api/js?libraries=geometry,places&amp;sensor=false\"></script>
        <script type=\"text/javascript\" src=\"https://subscribe.geoloqi.com/socket.io/socket.io.js\"></script>
        <script type=\"text/javascript\" src=\"http://api.geoloqi.com/js/geoloqi.min.js\"></script>
        
        <!-- Current Posistion -->
        <script language=\"JavaScript\">
          var LastLat=".$last_position["lat"].";
          var LastLong=".$last_position["long"].";
        </script>";  
     drupal_set_html_head($html_head_data);
     
     // Add the JavaScript for the map
     drupal_add_js(drupal_get_path('module', 'uscykel')."/uscykel_map.js");
     
     // Create a div where the map can reside
     $pagecontent = "<div id=\"map\" style=\"width: 550px; height:400px;\">Laddar karta. Vänta ...</div>";
     //$pagecontent .= "<div id=\"gps_lat\">0</div>";
     //$pagecontent .= "<div id=\"gps_long\">0</div>";
     
     // Create a table that will show data from the map.
     $pagecontent .= "<table width=\"550\"><tr><td width=\"50%\"><b>Nuvarande Plats:</b></td>
                      <td width=\"50%\"><b>Vädret Just Nu:</b></td></tr>
                      <tr><td><p><span id=\"StreetName\">Ingen karta har laddats</span></p>
                      <p><b>Hastighet:</b><br><span id=\"speed\">Okänd</span></td>
                      <td><span id=\"w_symbol\" style=\"text-align: center;\"></span><br /> 
                      <b>Temp:</b> <span id=\"w_temperature\"></span><br />
                      <b>Vind:</b> <span id=\"w_wind\"></span><br />
                      <b>Regn:</b> <span id=\"w_rain\"></span></td>
                      </table>";
     
     // Test run the task
     //$task = uscykel_theTask();
     
     //$pagecontent .= $task;
     
     // Return the content
     return $pagecontent;
  } else {
   // Dispaly the count down!
   // Add JavaScript for count down.
   drupal_add_js(drupal_get_path('module', 'uscykel')."/uscykel_tzcount.js");
   
   // Ad span for the countdown
   $pagecontent = "<div id=\"ticker\" style=\"text-align: center; height: 110px; width: 500px; font-size: 26px;\">
                    <p>Avgång om:</p><span id=\"tzcd\"></span></div>";
   
   // Return the page
   return $pagecontent;
  }
}


// Small page that will display the last location (as testing)
function uscykel_testlocationnode() {
   return "test";
}   