
// Function to load map when Window loads.
window.onload = function () {

  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 17,
    center: new google.maps.LatLng(LastLat, LastLong),
    mapTypeId: google.maps.MapTypeId.SATELLITE
  });
  
  // Preload location data
  $.get('/cykel/ahah/location/' + LastLat + ',' + LastLong, null, locationResponse);

  geoloqi.maps.setDefault(map);

  var drunkenBicycle = null;

  var testSocket = new geoloqi.Socket('trip', 'FN5GcUy');

  testSocket.events.location = function(location) {
    if(drunkenBicycle === null) {
      drunkenBicycle = geoloqi.maps.pins.Basic({position: new google.maps.LatLng(location.latitude, location.longitude)});          
    }
    drunkenBicycle.moveTo(new google.maps.LatLng(location.latitude, location.longitude), true);
    //document.getElementById('gps_lat').innerHTML = location.latitude;
    //document.getElementById('gps_long').innerHTML = location.longitude;
    
    // Get the Location data
    $.get('/cykel/ahah/location/' + location.latitude + ',' + location.longitude, null, locationResponse);
    document.getElementById('speed').innerHTML = location.speed + ' km/h';
  }

  testSocket.events.disconnect = function() {
    console.log('socket disconnected');
  }

  testSocket.start();

}

// Function for response.
var locationResponse = function(response) {
  var result = Drupal.parseJson(response);
  $('#StreetName').html(result.place);
  $('#w_symbol').html('<img src="http://api.met.no/weatherapi/weathericon/1.0/?symbol='+ result.w_symbol +';content_type=image/png" alt="Väderikon" style="vertical-align: middle;">')
  $('#w_temperature').html(' ' + result.w_temperature + ' grader');
  $('#w_wind').html(result.w_wind);
  $('#w_rain').html(result.w_rain + ' mm');
}


