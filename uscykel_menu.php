<?php

// Menu items and callbacks for the module.

function uscykel_menu() {
    $items['cykel'] = array(
      'page callback' => 'uscykel_mainnode',
      'title' => 'Göteborg - Uppsala på Cykel',
      'type' => MENU_CALLBACK,
      'access arguments' => array('access uscykel content'),
    );
    
    // AHAH page for location
    $items['cykel/ahah/location'] = array(
      'page callback' => 'uscykel_ahah_location',
      'type' => MENU_CALLBACK,
      'access arguments' => array('access uscykel content'),
    );
    
    // Page for showing the position.
    $items['cykel/lastposition'] = array(
      'page callback' => 'uscykel_lastposition',
      'title' => 'Last Position',
      'type' => MENU_CALLBACK,
      'access arguments' => array('access uscykel content'),
    );

    
    return $items;
}