<?php

// This file will run when Drupal is running its cron job.
function uscykel_cron() {
   // Runt the function
   // Check if we have passed the start date.
   // In that case we shall run the program
   //if (time() > strtotime("2012:07:20 10:00:00")){
   //  // Run the task!
   //  uscykel_theTask();
   //} 
   
   // DO NOT RUNA ANY LONGER !!!
   // We're home :)
  
}

// This is the function. We use it to run the task
function uscykel_theTask(){
   // Start by getting the last known position
   $last_position = uscykel_lastposition();
   
   // Get the latest position we have recorded in db.
   // If this is the same as loaded from geoloqi we shall not save.
   $db_query = "SELECT max(id) as id FROM {uscykel_locations}";
   $db_result = db_query($db_query);
   $max_id = db_fetch_object($db_result);
   
   // Get data from max_id
   $db_query = "SELECT timestamp, lat, lng FROM {uscykel_locations} WHERE id=".$max_id->id;
   $db_result = db_query($db_query);
   $last_db_position = db_fetch_object($db_result);
   
   // Make positions into easy read variables
   $db_latlong = $last_db_position->lat.",".$last_db_position->lng;
   $last_latlong = $last_position["lat"].",".$last_position["long"];
   
   // Check it.
   if ($last_latlong != $db_latlong){
     // This is a new position. Save it in DB.
   
     // Add this position to the database.
     $db_query = "INSERT INTO {uscykel_locations} (
                  `lat` ,
                  `lng` ,
                  `speed` ,
                  `altitude`
                  )
                  VALUES (
                  '".$last_position["lat"]."',
                  '".$last_position["long"]."',
                  '".$last_position["speed"]."',
                  '".$last_position["altitude"]."')";
     db_query($db_query);
   } 
  
   
  // Time for the weather. Let's load the last weather.
  $db_query = "SELECT max(id) as id FROM {uscykel_weather}";
  $db_result = db_query($db_query);
  $max_id = db_fetch_object($db_result);
  $db_query = "SELECT updated, temperature, rain FROM {uscykel_weather} WHERE id=".$max_id->id;
  $db_result = db_query($db_query);
  $last_weather = db_fetch_object($db_result);
    
  // Has the weather updatet in the last ten minutes?
  if (strtotime($last_weather->updated) < strtotime("-9 minutes")){ 
    // More than ten minutes since last weahter. 
     
    // Get current wheather and save it in the database.
    // First connect to YR.NO and download XML. 
    // Open data.
    $ch = curl_init ("http://api.met.no/weatherapi/locationforecast/1.8/?lat=".$last_position["lat"].";lon=".$last_position["long"]);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
    $xml_data = curl_exec ($ch);
  
    // Make the wheater data into an object
    $wheater_data = simplexml_load_string($xml_data);
  
    // Save relevant data in variables
    $lat = $wheater_data->product->time[0]->location["latitude"];
    $long = $wheater_data->product->time[0]->location["longitude"];
    $temperature = $wheater_data->product->time[0]->location->temperature["value"];
    $wind_speed = $wheater_data->product->time[0]->location->windSpeed["mps"];
    $wind_direction = $wheater_data->product->time[0]->location->windDirection["name"];
    $rain = $wheater_data->product->time[1]->location->precipitation["value"];
    $symbol = $wheater_data->product->time[1]->location->symbol["number"];
    
    // Save to the database.
    $db_query = "INSERT INTO {uscykel_weather} (
                  `lat` ,
                  `lng` ,
                  `temperature` ,
                  `wind` ,
                  `rain` ,
                  `symbol`
                  )
                  VALUES (
                  '$lat',
                  '$long',
                  '$temperature',
                  '$wind_speed m/sek $wind_direction',
                  '$rain',
                  '$symbol')";
    db_query($db_query);
     
    $return = $db_query; 
  }
  
  
  // Now it's time for the Tweet. 
  // Let us see when the last tweet was sent.
  $db_query = "SELECT max(id) as id FROM {uscykel_tweets}";
  $db_result = db_query($db_query);
  $max_id = db_fetch_object($db_result);
  $db_query = "SELECT updated FROM {uscykel_tweets} WHERE id=".$max_id->id;
  $db_result = db_query($db_query);
  $last_tweet = db_fetch_object($db_result);

  // Get the adress for the two types of tweets we can do.
  // First wee need to get the current adress.
  $position_address = uscykel_loadAdress($last_latlong);     
  
  // Start with the hourly update.
  // If no tweet has been sent within the last hour and the location hasen't
  // changed for the last two hours a tweet shall be sent.
    
  // Check the updatet.
  if (strtotime($last_tweet->updated) < strtotime("-59 minutes") AND strtotime($last_db_position->timestamp) < strtotime("-119 minutes")){
     // Begin with constructing the tweet.
     $tweet_text = "Vi är på ".$position_address["street_name"]." i "
                   .$position_address["town"]." kommun. Det är ".$last_weather->temperature." grader och ";
     
     // Check the amount of rain.
     if ($last_weather->rain == 0){
       $tweet_text .= "inget regn. ";
     } else {
       $tweet_text .= "det regnar ".$last_weather->rain." mm. ";
     }
     
     // Check our current speed. If more than zero include in tweet.
     if ($last_position["speed"] > 0){
       // The speed is more than zero.
       $tweet_text .= "Hastigheten är ".$last_position["speed"]." km/h ";
     }
     
     // Finnish tweet.
     $tweet_text .= "http://www.usvast.se/cykel #funkpol";
     
     // Send the tweet.
     $tweet_result = uscykel_post_tweet($tweet_text);

     // Save a copy in the database
     $db_query = "INSERT INTO {uscykel_tweets} (
                  `tweet` ,
                  `response`
                  )
                  VALUES (
                  '$tweet_text',
                  '$tweet_result')";
    db_query($db_query);
  }
  
  // Now lets see in what town we are. 
  // If it's a new one we shall tweet and save.
  $db_query = "SELECT count(*) FROM {uscykel_towns} WHERE name='".$position_address["town"]."'";
  $db_result = db_query($db_query);
  $town_found = db_result($db_result);
  
  // Have we been here before?
  if ($town_found == 0){
    // We have not.
    // Save this town in db.
     $db_query = "INSERT INTO {uscykel_towns} (
                  `lat` ,
                  `lng` ,
                  `name`
                  )
                  VALUES (
                  '".$last_position["lat"]."',
                  '".$last_position["long"]."',
                  '".$position_address["town"]."')";
    db_query($db_query);
    
    // Consturct a tweet for the ocation.
     $tweet_text = "Tjohoooo! Vi är nu i ".$position_address["town"]." kommun! Vi trampar på "
                   .$position_address["street_name"]." och just nu är ".$last_weather->temperature." grader. ";
     
     // Check the amount of rain.
     if ($last_weather->rain == 0){
       $tweet_text .= "Inget regn! ";
     } else {
       $tweet_text .= "Det regnar ".$last_weather->rain." mm. ";
     }
          
     // Finnish tweet.
     $tweet_text .= "http://www.usvast.se/cykel #funkpol";
     
     // Send the tweet.
     $tweet_result = uscykel_post_tweet($tweet_text);

     // Save a copy of the tweet
     $db_query = "INSERT INTO {uscykel_tweets} (
                  `tweet` ,
                  `response`
                  )
                  VALUES (
                  '$tweet_text',
                  '$tweet_result')";
    db_query($db_query);
    
    
  }
  
  
  
  // Return for debug
  return $return;

}


// Function for sending a tweet.
function uscykel_post_tweet($tweet_text) {

  // Use Matt Harris' OAuth library to make the connection
  // This lives at: https://github.com/themattharris/tmhOAuth
  require_once('tmhoauth/tmhOAuth.php');

      
  // Set the authorization values
  // In keeping with the OAuth tradition of maximum confusion, 
  // the names of some of these values are different from the Twitter Dev interface
  // user_token is called Access Token on the Dev site
  // user_secret is called Access Token Secret on the Dev site
  // The values here have asterisks to hide the true contents 
  // You need to use the actual values from Twitter
  $connection = new tmhOAuth(array(
    'consumer_key' => '',
    'consumer_secret' => '',
    'user_token' => '',
    'user_secret' => '',
  )); 

  
  // Make the API call
  $connection->request('POST', 
    $connection->url('1/statuses/update'), 
    array('status' => $tweet_text));
  return $connection->response['code'];
}
